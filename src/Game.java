import java.util.ArrayList;
import java.util.Scanner;

public class Game {
	private ArrayList <Board> board; ;
	private Player x; 
	private Player o;
	Scanner kb = new Scanner(System.in);
	
	Game () {
		this.x = new Player('x');
		this.o = new Player('o');
		this.board = new ArrayList <Board> () ;
	}
	
	private void printWelcome() {
		System.out.println("Welcome to OX game <3");
	}
	
	private void printTable() {
		char [] temp =  this.board.get(this.board.size()-1).getTable();
		for (int i = 0; i < temp.length ; i ++ ) {
			System.out.print(temp[i] + " ");
			if (i == 2 || i == 5 || i == 8) {
				System.out.println();
			}
		}
	}
	
	private void printTurn() {
		System.out.println("Turn " + this.board.get(this.board.size()-1).getCurrentPlayer().getName());
	}
	
	private void inputPosition() {
		while (true) {
			System.out.println("Please input Row, Col : ");
			try {
				int row = kb.nextInt();
				int col = kb.nextInt();
				int index = this.getRequirePosition(row, col);
				if ((row < 1 || row > 3) || (col < 1 || col > 3)) {
					System.out.println("Min value of Row and Col is 1 \n" + "Max value of Row and Col is 3");
					continue;
				}
				
				if (this.board.get(this.board.size()-1).checkBlank(index)) {
					System.out.println("Please change position!!");
					continue;
				}
				
				this.board.get(this.board.size()-1).setTable(index, this.board.get(this.board.size()-1).getCurrentPlayer().getName());
				break;
			} catch (Exception e){
				System.out.println("Position Incorrect");
				if (kb.hasNext()) {
					kb.nextLine();
				}
			}			
		}
	}
	
	private  void spacing() {
		System.out.println();
	}

	private void printWinner() {
		if (this.board.get(this.board.size()-1).getWinner() == null)
			System.out.println("!!!-----Draw-----!!!");
		else
			System.out.println("!!!-----"+this.board.get(this.board.size()-1).getWinner().getName() + " is the Winner"+"-----!!!");
	
		spacing();
	}
	
	private int getRequirePosition(int  row, int col) {
		return (row - 1) * 3 + col - 1;
	}
	
	private void play() {
		this.printWelcome();
		while (true) {
			this.printTable();
			this.printTurn();
			inputPosition();
			
			if (this.board.get(this.board.size()-1).isFinal())
				break;
		}
		printTable();
		printWinner();
	}
	
	public void start() {
		while (true) {
			newGame();
			play();
			if (this.watchWinRate()) {
				printWinRate();
			}
			if (!this.tryAgainQuestion()) {
				printWinRate();
				break;
			}			
		}
	}
	
	private boolean tryAgainQuestion() {
		char input;
		while (true) {
			System.out.println("Try Again? (Y/N)");
			input = kb.next().charAt(0);
			if (input == 'Y' || input == 'y' )
				return true;
			if (input == 'N' || input == 'n')
				return false;
		}
	}
	
	private void newGame() {
		this.board.add(new Board(this.x, this.o));
	}
	
	private void printWinRate() {
		System.out.println("Win Rate : ");
		System.out.println("Player " + this.x.getName() + " :  win [" + this.x.getWin()+"] lose [" + this.x.getLose()+ "] draw [" + this.x.getDraw() + "]");
		System.out.println("Player " + this.o.getName() + " :  win [" + this.o.getWin()+"] lose [" + this.o.getLose()+ "] draw [" + this.o.getDraw() + "]");
	}
	
	private boolean watchWinRate() {
		char input;
		while (true) {
			System.out.println("Do you want to watch Win Rate? (Y/N)");
			input = kb.next().charAt(0);
			if (input == 'Y' || input == 'y' )
				return true;
			if (input == 'N' || input == 'n')
				return false;
		}
	}
	

	

}
