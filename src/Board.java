public class Board {
	private  char [] table = { 	'-', '-', '-', 
						'-', '-', '-', 
						'-', '-', '-'  };
	private Player x;
	private Player o;
	private Player winner;
	private int turn;
	
	Board (Player x, Player o) {
		this.x = x;
		this.o = o;
		turn = 0;
	}
	
	public char[] getTable() {
		return this.table;
	}
	
	public Player getX() {
		return this.x;
	}
	
	public Player getO() {
		return this.o;
	}
	
	public Player getWinner() {
		return this.winner;
	}
	
	public Player getCurrentPlayer() {
		if (turn % 2 == 0)
			return this.x;
		return this.o;
	}
	
	public boolean checkBlank(int index) {
		if (table[index] == '-')
			return false;
		return true;
	}
	
	private boolean checkResult( int index, int diff) {
		if (table[index] == table[index + diff] && table[index + diff] == table[index + (diff * 2)])
			return true;
		return false;
	}
	
	private boolean checkResultSlant() {
		if (checkBlank(0) && checkResult(0, 4)) 
			return true;
		if (checkBlank(2) && checkResult(2, 2)) 
			return true;
		return false;
	}
	
	private boolean checkResultVertical() {
		if (checkBlank(0) && checkResult(0, 1)) 
			return true;
		if (checkBlank(3) && checkResult(3, 1)) 
			return true;
		if (checkBlank(6) && checkResult(6, 1)) 
			return true;
		return false;
	}
	
	private boolean checkResultHorizontal() {
		if (checkBlank(0) && checkResult(0, 3)) 
			return true;
		if (checkBlank(1) && checkResult(1, 3)) 
			return true;
		if (checkBlank(2) && checkResult(2, 3)) 
			return true;
		return false;
	}

	private boolean checkWin() {
		if (checkResultVertical())
			return true;
		if (checkResultHorizontal())
			return true;
		if (checkResultSlant())
			return true;
		return false;
		
	}
	
	private boolean checkDraw() {
		if (turn == 8) 
			return true;
		return false;
	}
	
	public void setTable(int index, char item) {
		this.table[index] = item;
	}
	
	private void switchTurn() {
		this.turn++;
	}
	
	public boolean isFinal() {
		if (checkWin())  {
			setWinner();
			setResulttoPlayer();
			return true;
		}
		if (checkDraw()) {
			setResulttoPlayer();
			return true;
		}
		
		switchTurn();
		return false;
		
	}
	
	private void setWinner() {
		this.winner = this.getCurrentPlayer();
	}
	
	private void setResulttoPlayer() {
		if (this.winner == null){ 
			this.x.setDraw(this.x.getDraw() + 1);
			this.o.setDraw(this.o.getDraw() + 1);
		}else if (this.winner.getName() == this.x.getName()) {
			this.x.setWin(this.x.getWin() + 1);
			this.o.setLose(this.o.getLose() + 1);
		} else  {
			this.o.setWin(this.o.getWin() + 1);
			this.x.setLose(this.x.getLose() + 1);
		}
	}

}
