
public class Player {
	private char name;
	private int win;
	private int draw;
	private int lose;
	
	Player (char name) {
		this.name = name;
		this.win = 0;
		this.draw = 0;
		this.lose = 0;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getLose() {
		return lose;
	}

	public void setLose(int lose) {
		this.lose = lose;
	}
	
}
